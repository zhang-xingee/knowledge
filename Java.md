# 简介
1. 什么是Java
- Java是一个object-oriented面向对象的计算机编程语言。
James Gosling 詹姆斯·高斯林是Java之父。
2. 可以用来做什么
- 桌面应用（使用AWT和Swing技术）
- Web应用（使用Servlet, JSP, Struts, Spring, Hibernate, JSF等技术）
- 企业应用（使用EJB）
- 移动应用（使用Android、Java ME）

# 历史
- 1. James Gosling, Mike Sheridan 和 Patrick Naughton 在1991年6月开启了Java语言项目。
- 2. JDK 1.0在1996年1月23日发布了。
- 3. JDK 8在2014年3月18日发布了。这也是目前最常使用的开发版本。

# 特性
1. 面向对象
* 封装
* 继承
* 多态
2. 平台独立
- Java提供了基于软件的平台JVM。Write Once and Run Anywhere 一次编写，到处运行。
3. 分布式
- 运用了RMI和EJB技术，使我们能够通过从互联网上的任何机器上调用方法来访问文件。
4. 等等



